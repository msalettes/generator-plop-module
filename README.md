# generator-plop-module [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> Yeoman generator for creating plop module

## Installation

First, install [Yeoman](http://yeoman.io) and generator-plop-module using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
npm install -g generator-plop-module
```

Then generate your new project:

```bash
yo plop-module
```

## Getting To Know Yeoman

 * Yeoman has a heart of gold.
 * Yeoman is a person with feelings and opinions, but is very easy to work with.
 * Yeoman can be too opinionated at times but is easily convinced not to be.
 * Feel free to [learn more about Yeoman](http://yeoman.io/).

## License

MIT © [Matthieu SALETTES]()


[npm-image]: https://badge.fury.io/js/generator-plop-module.svg
[npm-url]: https://npmjs.org/package/generator-plop-module
[travis-image]: https://travis-ci.org/msalettes/generator-plop-module.svg?branch=master
[travis-url]: https://travis-ci.org/msalettes/generator-plop-module
[daviddm-image]: https://david-dm.org/msalettes/generator-plop-module.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/msalettes/generator-plop-module
